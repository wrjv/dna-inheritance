#!/usr/bin/env python3

'''
Code written by Willem R. J. Vermeulen.

This file can be used to generate statistics on the shared DNA between
two cousins given a certain relationship.
'''


import logging
import itertools
from math import ceil

import matplotlib.pyplot as plt
import numpy as np

from Person import Person

logging.basicConfig(level=logging.INFO)

class MatchCousins():

    def __init__(self, nth_generation_self: int, nth_generation_other: int):
        '''
        Initialise

        VARIABLES
        =======================================================================
        nth_generation_self           Integer
            The number of generations of ancestors to generate for yourself. A
            greatgrandfather is a 4th generation ancestor.
        nth_generation_other          Integer
            The number of generations of ancestors to generate for the other
            person. A greatgrandfather is a 4th generation ancestor.
        '''

        if nth_generation_self > 30 or nth_generation_other > 30:
            raise ValueError('The number of generations must be between 1' +\
                             ' and 30.')

        self.nth_generation_a = nth_generation_self
        self.nth_generation_b = nth_generation_other

        self.individuals_a = []
        self.individuals_b = []

    def generate(self, sample_size: int):
        '''
        Generate all samples for the first and second individual.

        VARIABLES
        =======================================================================
        sample_size                 Integer
            The number of times the generations of ancestors have to be
            generated.
        '''

        for i in range(sample_size):

            if not i % (sample_size / 200):
                logging.info(str(round(i / sample_size * 100, 1)).rjust(6) +\
                             '% of the samples has been generated.')

            # Generate individuals "a" with "a" generations and "b" with "b" 
            # generations.
            self.individuals_a.append(Person('M', self.nth_generation_a))
            self.individuals_b.append(Person('M', self.nth_generation_b))

    def find_specific_matches(self, list_of_ancestors_self,
                              list_of_ancestors_other, plot=False, title='',
                              binwidth=10):
        '''
        Find specific matches between certain IDs of ancestors of the two
        persons.

        VARIABLES
        =======================================================================
        list_of_ancestors_self      List of Strings
            The IDs of ancestors of person 1.
        list_of_ancestors_other     List of Strings
            The IDs of ancestors of person 2.
        plot                        Boolean
            Whether the data should be plotted directly, or returned instead.
        title                       String
            Title used in the plot.
        binwidth                    Float
            Width of the bins used in the histogram (in cM).
        '''

        # This is needed because we want to be able to distinguish paternal and maternal side.
        list_a = sum([[person + 'M', person + 'F'] for person in list_of_ancestors_self], [])
        list_b = sum([[person + 'M', person + 'F'] for person in list_of_ancestors_other], [])

        dna = []

        for ind_a in self.individuals_a:
            for ind_b in self.individuals_b:
                dna.append(ind_a.distance(ind_b, list_a, list_b))

        if not plot:
            return dna

        # Plot
        plt.title(title)

        plt.hist(dna,
                 bins=range(0,
                            ceil(max(dna) / binwidth) *\
                                     binwidth + binwidth, binwidth),
                 alpha=0.5,
                 weights=np.ones(len(dna)) /\
                                 len(dna) * 100)


        plt.legend()
        plt.ylabel("Matches (%)")
        plt.xlabel("Shared DNA (cM)")
        plt.show()

    def generate_ancestors(self, generation):
        '''Generate the IDs of all ancestors in a specified generation'''

        keywords = [''.join(i) for i in itertools.product(['M', 'F'], repeat=generation - 2)]
        return [[i + 'M', i + 'F'] for i in keywords]

    def find_all_matches(self, half_sibling=False, plot=False, title='', binwidth=10):
        '''
        Find matches between two persons having a shared ancestor in the
        nth generation of person a and nth generation of person b, whilst
        indicating whether the siblings they descended from were half-siblings
        or not.

        VARIABLES
        =======================================================================
        half_siblings               Boolean
            Whether the siblings they descended from were half-siblings or not.
        plot                        Boolean
            Whether the data should be plotted directly, or returned instead.
        title                       String
            Title used in the plot.
        binwidth                    Float
            Width of the bins used in the histogram (in cM).
        '''

        ancestors_a = self.generate_ancestors(self.nth_generation_a)
        ancestors_b = self.generate_ancestors(self.nth_generation_b)

        dna = []

        if half_sibling:
            ancestors_a = [[a[0]] for a in ancestors_a] + [[a[1]] for a in ancestors_a]
            ancestors_b = [[b[0]] for b in ancestors_b] + [[b[1]] for b in ancestors_b]

        for set_a in ancestors_a:
            for set_b in ancestors_b:
                dna += self.find_specific_matches(set_a, set_b)

        if not plot:
            return dna

        plt.title(title)

        plt.hist(dna,
                 bins=range(0,
                            ceil(max(dna) / binwidth) *\
                                     binwidth + binwidth, binwidth),
                 alpha=0.5,
                 weights=np.ones(len(dna)) /\
                                 len(dna) * 100)

        plt.legend()
        plt.ylabel("Matches (%)")
        plt.xlabel("Shared DNA (cM)")
        plt.show()


def main():
    grinder = MatchCousins(3, 3)
    grinder.generate(50)

    #grinder.find_all_matches(plot=True)
    #grinder.find_all_matches(plot=True, half_sibling=True)

    #grinder.find_specific_matches(["MM", "MM"], ["FM", "FM"], plot=True)

    grinder.find_specific_matches(["FF", "FM"], ["FF", "FM"], plot=True)

if __name__ == '__main__':
    main()
