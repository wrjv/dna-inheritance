#!/usr/bin/env python3

'''
Code written by Willem R. J. Vermeulen.

This file represents a Person with DNA that can be traced for a certain number
of generations. For the Person it is possible to determine which segments come
from which ancestor. Each ancestor is a Person as well.
'''

import numpy as np


class Person:
    ''' Defines a person and what DNA they inherited from which ancestors. '''

    # Different companies use different numbers of centiMorgans.
    centimorgans = {"GEDmatch": 3783,
                    "FamilyTreeDNA": 3580.24,
                    "23andMe": 3719.4}

    def __init__(self, sex, generations, name="", statistics="FamilyTreeDNA"):
        '''
        Create a person and generate their DNA.

        VARIABLES
        =======================================================================
        sex                         String
            The sex of the person, must be 'M' or 'F'.
        generations                 Integers
            The number of generations of ancestors that need to be shown in
            the DNA of the person.
        name                        String
            Used to store any information about the name of a person (mostly
            for convenience), defaults to "".
        statistics                  String
            How many centiMorgans does a person have? Using statistics from
            either GEDmatch, FamilyTreeDNA or 23andMe.
        '''

        if sex not in ['M', 'F']:
            raise ValueError('The sex of a person must either be "M" or "F"')

        # Variables that give us information about the person.
        self.sex = sex
        self.name = name
        self.dna = {}
        self.statistics = statistics

        # Variables that give us information about the parents.
        self.father = None
        self.mother = None

        # If there is no need for known parents, this is the starting point for
        # that particular string of DNA.
        if generations == 1:
            self.__generate_dna__()
        # Otherwise, recursively generate the parents and generate own DNA
        # based on the DNA of the parents.
        else:
            self.father = Person('M', generations - 1, name + "M", statistics)
            self.mother = Person('F', generations - 1, name + "F", statistics)
            self.dna = self.father.create_child(self.mother, sex)

    def create_child(self, other, sex):
        '''
        Used to generate DNA in lower levels of the tree, in which case the
        DNA from the both sides needs further specification. To do this, DNA
        from both parents is combined, which creates the DNA of a new child.

        RETURNS
        =======================================================================
            The DNA of a new child, with certain segments linked to certain
            ancestors (Dictionary).
        '''

        # Creates a child if "self" is a male and other is a female, otherwise
        # make sure to swap the parents.
        if self.sex == "F":
            return other.create_child(self, sex)
        if self.sex == other.sex:
            return None

        return {"M": self.__combine_dna__(), "F": other.__combine_dna__()}

    def __generate_dna__(self):
        '''
        Gnerates DNA at the top level of the tree, in which case the
        DNA from the fathers side is just DNA from the fathers side and DNA
        from the mothers side is just DNA from the mothers side, without any
        further specification.
        '''

        # Create two empty dna strings.
        self.dna["M"] = [[0, self.centimorgans[self.statistics],
                          self.name + "M"]]
        self.dna["F"] = [[0, self.centimorgans[self.statistics],
                          self.name + "F"]]

    def __combine_dna__(self):
        '''
        Combines the DNA of the paternal and maternal side into a new string
        of DNA that can be given to a child.
        '''

        dna_outcome = []
        grandparent = np.random.choice(["M", "F"])

        # The number of times DNA recombines (switches between DNA of paternal
        # and maternal origins) differs between males and females. On average,
        # male DNA splits 27.7 times, while female DNA splits 41.7 times.
        #
        # Source:
        # Hussin J, Roy-Gagnon MH, Gendron R, Andelfinger G, Awadalla P.
        # Age-dependent recombination rates in human pedigrees.
        # PLoS Genet. 2011 Sep7(9):e1002251. doi: 10.1371/journal.pgen.1002251
        # Page 2, left column, 3rd paragraph.

        random_dna_splits = np.random.random_sample(28 if self.sex == "M"
                                                    else 42)
        random_dna_splits = list(np.array(sorted(random_dna_splits)) *
                                 self.centimorgans[self.statistics])

        split_list = [-1] + random_dna_splits +\
            [self.centimorgans[self.statistics]]

        # Perform genetic recombination based on the given splits.
        for segment in [(split_list[i], split_list[i + 1])
                        for i in range(len(split_list) - 1)]:
            dna_outcome += self.__get_dna__(grandparent, segment)
            grandparent = "M" if grandparent == "F" else "F"

        return dna_outcome

    def __get_dna__(self, grandparent, large_seg):
        '''
        Gathers information on all smaller segments (inherited from earlier
        ancestors) within a larger segment of DNA.

        VARIABLES
        =======================================================================
        grandparent                 String
            Whether we look at the paternal or maternal side, must be 'M' or
            'F'.
        large_seg                   [Float, Float]
            The boundaries of the larger segment specified by two Floats.
        '''

        small_segs = []

        for small_seg in self.dna[grandparent]:
            # If the smaller segment is not located within the larger segment,
            # we want to ignore it.
            if large_seg[0] > small_seg[1] or large_seg[1] < small_seg[0]:
                continue

            # If the smaller segment is located within the larger segment:

            # Find the start of the smaller segment, and make sure it doesn't
            # start before the larger segment.
            start = small_seg[0]
            if large_seg[0] > small_seg[0]:
                start = large_seg[0]

            # Find the end of the smaller segment, and make sure it doesn't
            # end after the larger segment.
            end = small_seg[1]
            if large_seg[1] < small_seg[1]:
                end = large_seg[1]

            small_segs.append([start, end, small_seg[2]])

        return small_segs

    def distance(self, other, ancestor_self, ancestor_other, min_cm=7):
        '''
        Find the genetic distance (cM) between two Persons when they both
        share ancestors, given the ancestors they share.

        VARIABLES
        =======================================================================
        other                       Person
            The other Person we want to compare ancestors with.
        ancestor_self               List
            List of ancestors of the current Person that should match with the
            ancestors of the other Person given in ancestor_self. Should be
            specified in the format ['MM', 'MF']
        ancestor_other              List
            List of ancestors of the other Person that should match with the
            ancestors of the current Person given in ancestor_self. Should be
            specified in the format ['MM', 'MF']
        min_cm                      Float
            The minimum segment size that needs to be accounted for. Should
            not be set too low in certain cases, as real life DNA testing
            companies will often discard smaller segments. Defaults to 7.

        RETURNS
        =======================================================================
            The genetic distance between both Persons (Float).
        '''

        if len(ancestor_self) != len(ancestor_other):
            raise ValueError("The same number of ancestors must be specified" +
                             " for both persons.")

        translate = {ancestor_self[i]: ancestor_other[i]
                     for i in range(len(ancestor_self))}

        distance = 0

        for part_self in self.dna["M"] + self.dna["F"]:
            if part_self[2] not in translate:
                continue

            for part_other in other.dna["M"] + other.dna["F"]:
                if part_other[2] != translate[part_self[2]] or\
                   part_other[1] < part_self[0] or\
                   part_other[0] > part_self[1]:
                    continue

                start = max(part_self[0], part_other[0])
                end = min(part_self[1], part_other[1])

                # Assure the minimum segment length.
                if end - start > min_cm:
                    distance += end - start

        return distance

    def get_statistics(self):
        '''
        Gathers and returns statistics on shared DNA for each ancestor.

        RETURNS
        =======================================================================
            The genetic distance between both Persons (dictionary,
            ancestor - shared cM pairs).
        '''

        statistics = {}

        for segment in self.dna['M'] + self.dna['F']:
            if segment[2] not in statistics:
                statistics[segment[2]] = 0

            statistics[segment[2]] += (segment[1] - segment[0])

        return statistics

    def __str__(self):
        '''
        Provides the name of the person.
        '''

        if self.name == "":
            return "Person (Unknown)"

        return "Person (" + str(self.name) + ")"


def main():
    ''' Several examples on relationships.'''

    person1 = Person('M', 3)
    person2 = Person('M', 3)
    print(person1.distance(person2, ["MMM", "MMF"], ["MMM", "MMF"], 0))
    print(person1.get_statistics())


if __name__ == '__main__':
    main()
