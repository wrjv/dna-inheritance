#!/usr/bin/env python3

'''
Code written by Willem R. J. Vermeulen.

This file can be used to generate statistics on the shared DNA between a
person and their ancestors. See:
- https://computationalhistory.com/shared-dna-across-generations/
'''


import csv
import itertools
import logging
import os
import random

from math import ceil

import matplotlib.pyplot as plt
import numpy as np

from scipy import stats
from Person import Person

logging.basicConfig(level=logging.INFO)


class MatchAncestors():

    def __init__(self, nth_generation: int):
        '''
        Initialises the matching program.

        VARIABLES
        =======================================================================
        nth_generation              Integer
            The number of generations of ancestors to generate. A
            greatgrandfather is a 4th generation ancestor. Must be between 1
            and 30 (because of performance limitations)
        '''

        if nth_generation > 30:
            raise ValueError('The number of generations must be between 1' +
                             ' and 30.')

        self.nth_generation = nth_generation
        self.combined_statistics = {}
        self.total_dna = []
        self.datasets = []

    def generate(self, sample_size: int, unique=False):
        '''
        Generates multiple family trees to allow for further distribution
        research.

        VARIABLES
        =======================================================================
        sample_size                 Integer
            The number of ancestors that have to be generated.
        unique                      boolean
            Whether you are looking to examine unique relationships (i.e.
            paternal grandfather and maternal grandmother) instead of just
            generations in general.
        '''

        # As a person has, for instance, 4 grandparents, only 1 person needs to
        # be generated to gather information on 4 grandparents.
        no_of_persons = ceil(float(sample_size) /
                             2 ** (self.nth_generation - 1))

        # If we, however, want to examine one specific relationship, we will
        # have to generate that relationship sample_size times.
        if unique:
            no_of_persons = sample_size

        for i in range(no_of_persons):
            if not i % (no_of_persons / 200):
                logging.info(str(round(i / no_of_persons * 100, 1)).rjust(6) +
                             '% of the samples has been generated.')

            # Generate a person with n-1 generations above him.
            person = Person('M', self.nth_generation - 1)

            person_statistics = person.get_statistics()

            for ancestor in [''.join(i) for i in itertools.
                             product(['M', 'F'],
                                     repeat=self.nth_generation - 1)]:

                # Make sure that there already is a bin available.
                if ancestor not in self.combined_statistics:
                    self.combined_statistics[ancestor] = []

                # If the person shares DNA with an ancestor, add the number in.
                if ancestor in person_statistics:
                    self.combined_statistics[ancestor]\
                        .append(person_statistics[ancestor])
                # If the person doesn't share DNA with an ancestor, add a 0 in.
                else:
                    self.combined_statistics[ancestor].append(0)

        # Combine all shared dna from ancestors into a single sorted list.
        for ancestor in self.combined_statistics:
            self.total_dna += self.combined_statistics[ancestor]

        self.total_dna.sort()

    def save_generation(self):
        '''
        Saves all generated shared cM data for the generation. Will overwrite
        any existing files that was previously generated.
        '''

        # check if folder already exists.
        if not os.path.exists('data'):
            os.makedirs('data')

        # Write the data away in a csv-file in the "data" folder.
        with open("../data/ancestors_generation_" + str(self.nth_generation) +
                  ".csv", 'w') as datafile:

            datawriter = csv.writer(datafile, quoting=csv.QUOTE_ALL)
            datawriter.writerow(self.total_dna)

    def __find_percentage__(self, percentage, confidence=0.95):
        '''

        VARIABLES
        =======================================================================
        percentage                  Float

        confidence                  Float
            Range of the confidence interval, two-sided. Defaults to 0.95.

        RETURNS
        =======================================================================
            A string that contains information about the mean and confidence
            interval for a certain percentage of the shared cM distrubtion
            between a person and their ancestors.
        '''

        index = round(len(self.total_dna) * (percentage / 100)) - 1

        sample_values = []
        for dataset in self.datasets:
            sample_values.append(dataset[index])

        # Determine the minimum and maximum value of the confidence interval.
        samples = len(self.datasets)
        sample_values.sort()

        min_value = sample_values[round(((1 - confidence) / 2) * samples)]
        max_value = sample_values[round((confidence +
                                         (1 - confidence) / 2) * samples)]

        return str(round(self.total_dna[index], 2)) + " cM, " + \
           str(round(confidence * 100, 2)) + "% CI (" +\
           str(round(min_value, 2)) + ", " +\
           str(round(max_value, 2)) + ")"

    # pylint: disable=dangerous-default-value
    def save_statistics(self, percentages=[25, 50, 75], confidence=0.95,
                        samples=500):
        '''
        Save statistics on inherited DNA to a file.

        VARIABLES
        =======================================================================
        percentages                 list of Floats
            Percentages of the shared DNA distribution for which the amount of
            shared cM is shown.
        confidence                  Float
            Confidence interval used in determining the shared cM.
        samples                     Integer
            Number of samples used in determining the shared cM.
        '''

        # Check if percentages used are a list.
        if not isinstance(percentages, tuple) and\
           not isinstance(percentages, list):
            raise TypeError("percentages should be a tuple or list.")

        # Generate several datasets to be able to acquire confidence intervals.
        logging.info("Generating datasets...")
        for i in range(0, samples):
            dataset = random.choices(self.total_dna, k=len(self.total_dna))
            dataset.sort()
            self.datasets.append(dataset)
            logging.info("\tDataset" + str(i))

        # Generate general statistics.
        output = "*" * 80 + "\n"
        output += "Generation " + str(self.nth_generation) + "\n\n"
        output += "Samples: " + str(len(self.total_dna)) + "\n"
        output += "Percentage no match: " +\
            str(len([i for i in self.total_dna if i == 0]) /
                len(self.total_dna) * 100) + "%\n"
        output += "*" * 80 + "\n"

        # Generate info about certain points of the shared DNA range.
        for percentage in percentages:
            output += str(percentage) + "% : " +\
                self.__find_percentage__(percentage, confidence) + " \n"

        output += "*" * 80 + "\n"

        # check if folder already exists.
        if not os.path.exists('output'):
            os.makedirs('output')

        # Save statistics
        text_file = open("../output/ancestors_generation_" +
                         str(self.nth_generation) + "_log.txt", "w")
        text_file.write(output)
        text_file.close()

    def plot_generation(self, title='', binwidth=10, set_range=True,
                        plot_zero=True, save=False):
        '''
        Plot the distribution of shared DNA between a person and all ancestors
        in the current generation.

        VARIABLES
        =======================================================================
        title                       String
            Title used int he plot.
        binwidth                    Float
            Width of the bins used in the histogram (in cM).
        set_range
            Whether the plot ranges should be limited to (3000, binwidth * 2)
        plot_zero                   Boolean
            Whether to also plot matches that share 0 cM.
        save
            Whether the resulting plot should also be saved.
        '''

        # Set default title when no title is specified.
        if not title:
            title = 'Shared DNA (cM) with own '

            if self.nth_generation == 3:
                title += 'grandparents'
            elif self.nth_generation == 4:
                title += 'greatgrandparents'
            elif self.nth_generation > 4:
                title += str(self.nth_generation - 3) +\
                    'x-greatgrandparents'

        plt.figure()
        plt.title(title)

        # Add up statistics for all ancestors in a generation.
        total_dna = self.total_dna

        if not plot_zero:
            total_dna = [item for item in total_dna if item != 0]

        # Plot the histogram.
        plt.hist(total_dna,
                 bins=range(0,
                            ceil(max(total_dna) / binwidth) * binwidth +
                            binwidth, binwidth),
                 alpha=0.5,
                 weights=np.ones(len(total_dna)) / len(total_dna)*100)
        plt.ylabel("Matches (%)")
        plt.xlabel("Shared cM with ancestor (cM)")

        if set_range:
            plt.xlim(0, 3000)
            plt.ylim(0, binwidth * 2)

        if save:
            # check if folder already exists.
            if not os.path.exists('output'):
                os.makedirs('output')

            plt.savefig("../output/ancestors_generation_" +
                        str(self.nth_generation) + ".png")
        else:
            plt.show()

    def plot(self, ancestors, names, title='', binwidth=10):
        '''
        Plot histograms that show the distributions of shared DNA for different
        ancestors.

        VARIABLES
        =======================================================================
        ancestors                   List of Strings
            Identifying codes for the ancestors. 
        names                       List of Strings
            Identifying names for the ancestors. Should be as long as the
            other list, "ancestors".
        title                       String
            Title used in the plot.
        binwidth                    Float
            Width of the bins used in the histogram (in cM).
        '''

        plt.title(title)

        for ancestor, name in zip(ancestors, names):
            plt.hist(self.combined_statistics[ancestor],
                     bins=range(0,
                                ceil(max(self.combined_statistics[ancestor]) /
                                     binwidth) * binwidth + binwidth,
                                binwidth),
                     alpha=0.5, label=str(name),
                     weights=np.ones(len(self.combined_statistics[ancestor])) /\
                                     len(self.combined_statistics[ancestor]) * 100)

        plt.legend()
        plt.ylabel("Matches (%)")
        plt.xlabel("Shared cM with ancestor (cM)")
        plt.show()

    def kolmogorov(self):
        '''
        Determine whether the generated paternal and maternal shared DNA
        distributions for the used generation are statistically different.
        '''

        return stats.ks_2samp(self.combined_statistics[(self.nth_generation
                                                        - 1) * 'M'],
                              self.combined_statistics[(self.nth_generation
                                                        - 1) * 'F'])


def main():
    for generation in range(3, 11):
        grinder = MatchAncestors(generation)
        grinder.generate(1000000)
        grinder.save_statistics([0.5, 2.5, 25, 50, 75, 97.5, 99.5], 0.95, 200)
        grinder.save_generation()
        grinder.plot_generation(binwidth=5, set_range=False, save=True)

    #grinder.plot(['MMMMM', 'FFFFF'], ['PATERNAL', 'MATERNAL'],
    #             "Difference between maternal and paternal line", 10)

    #grinder.plot(['MMMMMMM', 'FFFFFFF'], ['PATERNAL', 'MATERNAL'], 5)


if __name__ == '__main__':
    main()
